package org.loda.enumeration;

public enum PessimisticLock {

	//关闭
	CLOSE(0),
	//开启
	OPEN(1);
	
	private int status;
	
	private PessimisticLock(int status){
		this.status=status;
	}
	
	public int status(){
		return status;
	}
}
