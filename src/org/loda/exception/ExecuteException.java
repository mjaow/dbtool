package org.loda.exception;

public class ExecuteException extends RuntimeException {

	private static final long serialVersionUID = 1053168486101012556L;

	public ExecuteException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExecuteException(String message) {
		super(message);
	}

}
