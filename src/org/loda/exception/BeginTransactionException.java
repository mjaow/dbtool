package org.loda.exception;

public class BeginTransactionException extends RuntimeException {

	public BeginTransactionException(String msg, Throwable e) {
		super(msg, e);
	}

	private static final long serialVersionUID = 4785194364090863882L;

}
