package org.loda.exception;

public class NoPrimaryKeyException extends RuntimeException {

	public NoPrimaryKeyException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = 3290318097662772054L;

}
