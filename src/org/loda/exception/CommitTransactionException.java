package org.loda.exception;

public class CommitTransactionException extends RuntimeException {

	private static final long serialVersionUID = 3222253309858215956L;

	public CommitTransactionException(String message, Throwable cause) {
		super(message, cause);
	}

}
