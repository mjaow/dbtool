package org.loda.exception;

public class FieldNotFoundException extends RuntimeException {

	public FieldNotFoundException(String msg, Throwable e) {
		super(msg, e);
	}

	private static final long serialVersionUID = 6072243367405081822L;

}
