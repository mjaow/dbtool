package org.loda.exception;

public class InitDbConnectionException extends RuntimeException{

	private static final long serialVersionUID = -447439755399871795L;

	public InitDbConnectionException(String message, Throwable e) {
		super(message,e);
	}

	public InitDbConnectionException(String message) {
		super(message);
	}
}
