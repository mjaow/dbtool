package org.loda.util;

import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

	private static Properties ps=null;
	static{
		ps=new Properties();
		try {
			ps.load(ConfigReader.class.getResourceAsStream("/db.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getString(String name){
		return ps.getProperty(name);
	}
	
	public static boolean getBoolean(String name){
		return Boolean.parseBoolean(ps.getProperty(name));
	}
	
}
