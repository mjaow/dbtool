package org.loda.util;

public class LogUtil {

	private static boolean record=false;
	
	static{
		record=ConfigReader.getBoolean("record");
	}

	public static void recordLog(String msg){
		if(record){
			System.out.println(msg);
		}
	}
	
}
