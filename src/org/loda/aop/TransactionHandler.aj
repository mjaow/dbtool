package org.loda.aop;

import org.loda.annotation.Tx;
import org.loda.util.Db;

/**
 * 
 * @ClassName: TransactionAop
 * @Description: 事务的aop处理类
 * @author minjun
 * @date 2015年4月1日 下午10:02:23
 * 
 */
public aspect TransactionHandler {

	pointcut serviceMethod() : execution(@Tx * *(..));

	before() :  serviceMethod(){
		Db.begin();
	}
	
	after() returning() : serviceMethod() {
		Db.commit();
	}
	
	after() throwing() : serviceMethod() {
		Db.rollback();//回滚事务的同时也会重置事务所有属性
	}
}
