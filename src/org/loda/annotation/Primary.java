package org.loda.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
* @ClassName: Primary 
* @Description: 主键 
* @author minjun
* @date 2015年3月28日 下午11:58:01 
*
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Primary {

	boolean autoIncreament() default false;
}
