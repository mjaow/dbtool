package org.loda.test;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.loda.annotation.Table;
import org.loda.enumeration.PessimisticLock;
import org.loda.test.model.Product;
import org.loda.test.model.User;
import org.loda.util.ConfigReader;
import org.loda.util.Db;

public class TestCase {

	@Test
	public void t1() {
		// String u=ConfigReader.getString("url");
		// System.out.println(u);
		//
		// Db.save(new User());

		// List<User> users=Db.findAll(User.class);
		// aaa(new User());
		// System.out.println(new User().getClass().getSimpleName());
		User user = new User();
		Class<? extends User> c = user.getClass();
		Table t = c.getAnnotation(Table.class);
		System.out.println(t);
		// System.out.println(User.class.getDeclaredAnnotations().length);
	}

	@Test
	public void t2() {
		List<User> users = Db.findAll(User.class);
		for (User user : users)
			System.out.println(user);
	}

	@Test
	public void t3() throws Exception {
		User user = Db.findById(2, User.class);
		System.out.println(user);
	}

	@Test
	public void t4() {
		User user = new User();
		user.setId(1);
		System.out.println(Db.delete(user));
	}

	@Test
	public void t5() {
//		try {
//			Db.begin();
			User user = new User();
			user.setPassword("111");
			user.setUsername("sdfsadf");
			user.setHobby("dsfasdfsdafdafsadf");
			System.out.println(Db.save(user));
			System.out.println(1/0);
//			Db.commit();
//		} catch (Exception e) {
//			Db.rollback();
//		}
	}

	@Test
	public void t6() {
		try {
			Db.begin();
			User user = Db.findById(2, User.class);
			user.setPassword("aaaaa");
			System.out.println(Db.update(user));
			Db.commit();
		} catch (Exception e) {
			Db.rollback();
		}
	}

	public static void main(String[] args) {
		Executor executor = Executors.newCachedThreadPool();
		final TestCase t=new TestCase();
		for (int i = 0; i < 10; i++) {
			executor.execute(new Runnable() {

				@Override
				public void run() {
					t.service();
				}
			});
		}
	}
	
	public void service(){
		try {
			Db.begin();
			//悲观锁设定，select for update，在一个事物进行查询的时候，锁住当前记录
			Product p = Db.findById(1, Product.class,PessimisticLock.OPEN);
			int stock = p.getStock();
			if (stock <= 0) {
				System.out.println("库存不足，停止售卖");
			} else {
				p.setStock(--stock);
				Db.update(p);
			}
			Db.commit();
		} catch (Exception e) {
			e.printStackTrace();
			Db.rollback();
		} 
	}

	public static void aaa(Object o) {
		System.out.println(o.getClass());
	}
	
	@Test
	public void t7(){
		System.out.println(ConfigReader.getBoolean("record"));
	}
	
	@Test
	public void testAspectj(){
		TestService t=new TestService();
		t.save();
	}
}
