package org.loda.test.model;

import org.loda.annotation.Field;
import org.loda.annotation.Primary;
import org.loda.annotation.Table;

@Table("t_user")
public class User {

	@Primary(autoIncreament=true)
	@Field("t_id")
	private int id;
	
	@Field("t_username")
	private String username;
	
	@Field("t_password")
	private String password;
	
	@Field("t_hobby")
	private String hobby;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + ", hobby=" + hobby + "]";
	}
}
