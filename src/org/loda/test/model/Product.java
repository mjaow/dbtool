package org.loda.test.model;

import org.loda.annotation.Primary;
import org.loda.annotation.Table;

@Table("product")
public class Product {

	@Primary(autoIncreament=true)
	private int id;
	
	private String name;
	
	/**库存*/
	private int stock;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
}
